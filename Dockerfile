FROM python:3.8-slim
COPY . /app
WORKDIR /app
RUN pip install -r requirements.txt
EXPOSE 8000

CMD ["gunicorn", "-w 1", "-b :8000", "main:app"]